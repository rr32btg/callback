<?php
/**
 * Copyright (c) Alexander A Kusakin. All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alexander A Kusakin (rr32btg) <alexander.a.kusakin@gmail.com>, 2017
 *
 */

define('URL', 'http://10.12.93.1/conference/call_with_tag.php'); //edit with real path to call_with_tag.php

if (empty($_POST['num'])) {
    return json_encode(array('ok' => false, 'reason' => 'num is not provided'));
}

if (!function_exists('curl_init')) {
    return json_encode(array('ok' => false, 'reason' => 'curl unavailable'));
}

$num = preg_replace('/[\s\-\)\(\+]./', '', $_POST['num']);
$tag =  !empty($_POST['tag']) ? $_POST['tag'] : '"' . $num . '" <' . $num . '>';

$curl = curl_init();

curl_setopt($curl, CURLOPT_URL, URL);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, 'num=' . $num . '&tag=' . $tag);

$out = curl_exec($curl);

curl_close($curl);

echo $out;
