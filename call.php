<?php
/**
 * Copyright (c) Alexander A Kusakin. All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Alexander A Kusakin (rr32btg) <alexander.a.kusakin@gmail.com>, 2017
 *
 */

/////////////////////////
// edit settings here //
///////////////////////

define('CHANNEL_CONTEXT', 'from-manager');
define('CONTEXT', 'www-to-client');
define('TIME_OUT', '20000');
define('ASYNC', '1');
define('MAX_RETRIES', '2');
define('RETRY_TIME', '30');
define('PRIORITY', 1);

define('ENABLE_NOTIFICATIONS', false);
define('NOTIFICATION_EMAIL', 'user@example.org');

define('PBX_QUEUE_OR_USER_NUM', '555');

date_default_timezone_set("Europe/Moscow");

require_once('asterisk/phpagi-asmanager.php');

///////////////////////////////////////
/////      script logic           ////
/////////////////////////////////////

function terminate_script($status = false)
{
    echo json_encode(array("ok" => $status));
    exit($status ? 0 : 1);
}

// make_call - makes call throug ami 
// $from string - represents pbx's user num
// $to string - represents site user num
// $tag string - some string that will be shown as Callerid, if empy $to will be shown
function make_call($from = PBX_QUEUE_OR_USER_NUM, $to, $tag)
{
    $asm = new AGI_AsteriskManager();

    if ($asm->connect()) {
        $call = $asm->send_request('Originate', array(
                'Channel' => 'LOCAL/' . $from . '@' . CHANNEL_CONTEXT,
                'Context' => CONTEXT,
                'Exten' => $to,
                'Timeout' => TIME_OUT,
                'Async' => ASYNC,
                'MaxRetries' => MAX_RETRIES,
                'RetryTime' => RETRY_TIME,
                'Priority' => PRIORITY,
                'Callerid' => !empty($tag) !== '' ? $tag : '"' . $to . '" <' . $to . '>'
            )
        );
        $asm->disconnect();
        return $call === false ? false : true;
    }

    return false;
}

if (empty($_POST['num'])) {
    terminate_script();
}

$to = preg_replace('/[\s\-\)\(\+]./', '', $_POST['num']);

if (!preg_match('/^(02)?(8|\+7|7)[2-9]\d{7,16}$/', $to)) {
    terminate_script();
}

$status = make_call(PBX_QUEUE_OR_USER_NUM, $to, $_POST['tag']);

if(ENABLE_NOTIFICATIONS){
    mail(NOTIFICATION_EMAIL, 'callback', 'callback to ' . $to . ' ' . $status ? 'sended' : 'failed');
}

die(json_encode(array('ok' => $status)));

